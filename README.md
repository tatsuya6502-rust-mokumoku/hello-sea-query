# Hello SeaQuery

このサンプルRustプログラムは、SQLによるクエリー（主に`SELECT`文）を[SeaQuery][sea-query]で構築し、[SQLx][sqlx]で実行します。
SQL文は[データサイエンス100本ノック（構造化データ加工編）][100knocks]の解答例からお借りしています。

[sqlx]: https://github.com/launchbadge/sqlx
[sea-query]: https://github.com/SeaQL/sea-query
[100knocks]: https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess


## 実行方法

データサイエンス100本ノック（構造化データ加工編）の[Install][100knocks-install]に従って、Dockerコンテナーを起動してください。PostgreSQLとJupyterLabが起動し、データが投入されます。

[100knocks-install]: https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess#install

本リポジトリーをクローンして、`cargo run`で実行します。（Rustツールチェーンが必要です）

```console
$ git clone https://gitlab.com/tatsuya6502-rust-mokumoku/hello-sea-query.git
$ cd hello-sea-query

$ cargo run --release
```

データサイエンス100本ノックの設問と解答例は[ans_preprocess_knock_SQL.ipynb][100knocks-ans]で見られます。

[100knocks-ans]: https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess/blob/master/docker/work/answer/ans_preprocess_knock_SQL.ipynb
