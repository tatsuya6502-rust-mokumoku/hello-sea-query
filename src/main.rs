use core::fmt;

use anyhow::Context;
use envconfig::Envconfig;
use sea_query::{
    any, enum_def, Alias, Asterisk, CommonTableExpression, Expr, Func, Iden, Order,
    PostgresQueryBuilder, Query, SelectStatement, SimpleExpr, WindowStatement, WithClause,
};
use sea_query_binder::SqlxBinder;
use sqlx::types::BigDecimal;

/// `.env`の内容を読み込むための構造体
#[derive(Envconfig)]
pub struct Config {
    #[envconfig(from = "DATABASE_URL")]
    pub db_url: String,
}

/// データサイエンス100本ノック（構造化データ加工編）の解答例の一部からコピーしたSQL文と、
/// SeaQueryで構築した同様のSQL文のそれぞれを、SQLxで実行する。
///
/// データサイエンス100本ノックの情報は以下を参照。
///
/// - [ER図][100knocks-er]
/// - [解答例][100knocks-ans]
///
/// [100knocks-er]: https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess/blob/master/docker/doc/100knocks_ER.pdf
/// [100knocks-ans]: https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess/blob/master/docker/work/answer/ans_preprocess_knock_SQL.ipynb
#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // .envファイルの内容を環境変数に追加する
    let _ = envmnt::load_file(".env");
    let config = Config::init_from_env()?;

    // DB接続
    let db = sqlx::postgres::PgPoolOptions::new()
        .max_connections(5)
        .connect(&config.db_url)
        .await?;

    // SQLを実行
    s006(&db).await?;
    s029(&db).await?;
    s035(&db).await?;
    s036(&db).await?;

    Ok(())
}

// ---------------------------------------------------
// テーブル定義
// ---------------------------------------------------

/// `receipt`テーブルのカラム定義
#[enum_def]
pub struct Receipt {
    pub sales_ymd: i32,
    pub sales_epoch: Option<i32>,
    pub store_cd: String,
    pub receipt_no: i16,
    pub receipt_sub_no: i16,
    pub customer_id: Option<String>,
    pub product_cd: Option<String>,
    pub quantity: Option<i32>,
    pub amount: Option<i32>,
}

impl Receipt {
    /// `Receipt`構造体の定義がデータベースのスキーマ定義と一致していることを確認する。
    /// （SQLxによりコンパイル時にチェックされる）
    #[allow(unused)]
    async fn verify(db: &sqlx::PgPool) {
        sqlx::query_as!(
            Self,
            r#"
                SELECT
                    sales_ymd,
                    sales_epoch,
                    store_cd,
                    receipt_no,
                    receipt_sub_no,
                    customer_id,
                    product_cd,
                    quantity,
                    amount
                FROM receipt
                LIMIT 1
            "#
        )
        .fetch_one(db)
        .await
        .unwrap();
    }
}

/// `store`テーブルのカラム定義
#[enum_def]
pub struct Store {
    pub store_cd: String,
    pub store_name: Option<String>,
    pub prefecture_cd: Option<String>,
    pub prefecture: Option<String>,
    pub address: Option<String>,
    pub address_kana: Option<String>,
    pub tel_no: Option<String>,
    pub longitude: Option<BigDecimal>,
    pub latitude: Option<BigDecimal>,
    pub floor_area: Option<BigDecimal>,
}

impl Store {
    /// `Receipt`構造体の定義がデータベースのスキーマ定義と一致していることを確認する。
    /// （SQLxによりコンパイル時にチェックされる）
    #[allow(unused)]
    async fn verify(db: &sqlx::PgPool) {
        sqlx::query_as!(
            Self,
            r#"
                SELECT
                    store_cd,
                    store_name,
                    prefecture_cd,
                    prefecture,
                    address,
                    address_kana,
                    tel_no,
                    longitude,
                    latitude,
                    floor_area
                FROM store
                LIMIT 1
            "#
        )
        .fetch_one(db)
        .await
        .unwrap();
    }
}

// ---------------------------------------------------
// SQL関数でSeaQueryで定義されてないもの
// ---------------------------------------------------

struct Rank;

impl Iden for Rank {
    fn unquoted(&self, s: &mut dyn fmt::Write) {
        write!(s, "RANK").unwrap();
    }
}

// ---------------------------------------------------
// このアプリ内で使うトレイトの定義
// ---------------------------------------------------

pub trait FieldNames {
    fn field_names() -> &'static [&'static str];
}

// ---------------------------------------------------
// S-006
// ---------------------------------------------------

/// S-006の生SQLクエリーとSeaQueryで構築したクエリーを実行し、結果が同じであることを確認する。
async fn s006(db: &sqlx::PgPool) -> anyhow::Result<()> {
    println!("S-006");
    let rows1 = s006_raw(db).await?;
    let rows2 = s006_sea_query(db).await?;

    assert_eq!(rows1.len(), rows2.len());
    println!("{}", S006Row::field_names().join(", "));
    for (row1, row2) in rows1.into_iter().zip(rows2) {
        println!("{row2}");
        assert_eq!(row1, row2);
    }

    println!();
    Ok(())
}

async fn s006_raw(db: &sqlx::PgPool) -> anyhow::Result<Vec<S006Row>> {
    sqlx::query_as!(
        S006Row,
        r#"
            SELECT
                sales_ymd,
                customer_id,
                product_cd,
                quantity,
                amount
            FROM
                receipt
            WHERE
                customer_id = 'CS018205000001'
                AND
                (
                    amount >= 1000
                    OR quantity >= 5
                )"#
    )
    .fetch_all(db)
    .await
    .with_context(|| "raw query")
}

/// S-029のSQL文と同様のものをSeaQueryで構築し、SQLxで実行する。
async fn s006_sea_query(db: &sqlx::PgPool) -> anyhow::Result<Vec<S006Row>> {
    use ReceiptIden::*;

    let (sql, values) = Query::select()
        .columns([SalesYmd, CustomerId, ProductCd, Quantity, Amount])
        .from(Table)
        .and_where(Expr::col(CustomerId).eq("CS018205000001"))
        .cond_where(any![
            Expr::col(Amount).gte(1000),
            Expr::col(Quantity).gte(5)
        ])
        .build_sqlx(PostgresQueryBuilder);

    // SQL文をSQLxで実行する
    sqlx::query_as_with(&sql, values)
        .fetch_all(db)
        .await
        .with_context(|| format!("sea-query: {}", sql))
}

/// S-006のクエリー結果を格納する構造体
#[derive(sqlx::FromRow, Debug, PartialEq, Eq)]
pub struct S006Row {
    pub sales_ymd: i32,
    pub customer_id: Option<String>,
    pub product_cd: Option<String>,
    pub quantity: Option<i32>,
    pub amount: Option<i32>,
}

impl fmt::Display for S006Row {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}, {}, {}, {}, {}",
            self.sales_ymd,
            self.customer_id.clone().unwrap_or_default(),
            self.product_cd.clone().unwrap_or_default(),
            self.quantity.unwrap_or_default(),
            self.amount.unwrap_or_default(),
        )
    }
}

impl FieldNames for S006Row {
    fn field_names() -> &'static [&'static str] {
        &[
            "sales_ymd",
            "customer_id",
            "product_cd",
            "quantity",
            "amount",
        ]
    }
}

// ---------------------------------------------------
// S-029
// ---------------------------------------------------

/// S-029の生SQLクエリーとSeaQueryで構築したクエリーを実行し、結果が同じであることを確認する。
async fn s029(db: &sqlx::PgPool) -> anyhow::Result<()> {
    println!("S-029");
    let rows1 = s029_raw(db).await?;
    let rows2 = s029_sea_query(db).await?;

    assert_eq!(rows1.len(), rows2.len());
    println!("{}", S029Row::field_names().join(", "));
    for (row1, row2) in rows1.into_iter().zip(rows2) {
        println!("{row2}");
        assert_eq!(row1, row2);
    }

    println!();
    Ok(())
}

/// S-035の生SQLクエリーをSQLxで実行する。
async fn s029_raw(db: &sqlx::PgPool) -> anyhow::Result<Vec<S029Row>> {
    sqlx::query_as!(
        S029Row,
        r#"
            WITH product_cnt AS (
                SELECT
                    store_cd,
                    product_cd,
                    COUNT(1) AS mode_cnt
                FROM receipt
                GROUP BY
                    store_cd,
                    product_cd
            ),
            product_mode AS (
                SELECT
                    store_cd,
                    product_cd,
                    mode_cnt,
                    RANK() OVER(PARTITION BY store_cd ORDER BY mode_cnt DESC) AS rnk
                FROM product_cnt
            )
            SELECT
                store_cd,
                product_cd,
                mode_cnt
            FROM product_mode
            WHERE
                rnk = 1
            ORDER BY
                store_cd,
                product_cd
            LIMIT 10"#
    )
    .fetch_all(db)
    .await
    .with_context(|| "raw query")
}

/// S-029のSQL文と同様のものをSeaQueryで構築し、SQLxで実行する。
async fn s029_sea_query(db: &sqlx::PgPool) -> anyhow::Result<Vec<S029Row>> {
    use ReceiptIden::*;

    // 一時的なテーブルやカラムの名前
    let product_cnt = Alias::new("product_cnt");
    let mode_cnt = Alias::new("mode_cnt");
    let product_mode = Alias::new("product_mode");
    let rnk = Alias::new("rnk");

    // WITH句。このWITH句は共通表式（CTE: Common Table Expression）を2つ持つ
    let with = WithClause::new()
        .cte(
            CommonTableExpression::new()
                .table_name(product_cnt.clone())
                .query(
                    Query::select()
                        .column(StoreCd)
                        .column(ProductCd)
                        .expr_as(Func::count(Expr::val(1)), mode_cnt.clone())
                        .from(ReceiptIden::Table)
                        .group_by_columns([StoreCd, ProductCd])
                        .to_owned(),
                )
                .to_owned(),
        )
        .cte(
            CommonTableExpression::new()
                .table_name(product_mode.clone())
                .query(
                    Query::select()
                        .column(StoreCd)
                        .column(ProductCd)
                        .column(mode_cnt.clone())
                        .expr_window_as(
                            Func::cust(Rank),
                            WindowStatement::partition_by(StoreCd)
                                .order_by(mode_cnt.clone(), Order::Desc)
                                .to_owned(),
                            rnk.clone(),
                        )
                        .from(product_cnt)
                        .to_owned(),
                )
                .to_owned(),
        )
        .to_owned();

    // メインのSELECT文
    let select = Query::select()
        .column(StoreCd)
        .column(ProductCd)
        .column(mode_cnt)
        .from(product_mode)
        .and_where(Expr::col(rnk).eq(Expr::val(1)))
        .order_by_columns([(StoreCd, Order::Asc), (ProductCd, Order::Asc)])
        .limit(10)
        .to_owned();

    // SQL文を生成しSQLxで実行する
    let (sql, values) = select
        .with(with)
        .to_owned()
        .build_sqlx(PostgresQueryBuilder);
    sqlx::query_as_with(&sql, values)
        .fetch_all(db)
        .await
        .with_context(|| format!("sea-query: {}", sql))
}

/// S-029のクエリー結果を格納する構造体
#[derive(sqlx::FromRow, Debug, PartialEq, Eq)]
pub struct S029Row {
    pub store_cd: String,
    pub product_cd: Option<String>,
    pub mode_cnt: Option<i64>,
}

impl FieldNames for S029Row {
    fn field_names() -> &'static [&'static str] {
        &["store_cd", "product_cd", "mode_cnt"]
    }
}

impl fmt::Display for S029Row {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}, {}, {}",
            self.store_cd,
            self.product_cd.clone().unwrap_or_default(),
            self.mode_cnt.unwrap_or_default(),
        )
    }
}

// ---------------------------------------------------
// S-035
// ---------------------------------------------------

/// S-035の生SQLクエリーとSeaQueryで構築したクエリーを実行し、結果が同じであることを確認する。
async fn s035(db: &sqlx::PgPool) -> anyhow::Result<()> {
    println!("S-035");
    let rows1 = s035_raw(db).await?;
    let rows2 = s035_sea_query(db).await?;

    assert_eq!(rows1.len(), rows2.len());
    println!("{}", S035Row::field_names().join(", "));
    for (row1, row2) in rows1.into_iter().zip(rows2) {
        println!("{row2}");
        assert_eq!(row1, row2);
    }

    println!();
    Ok(())
}

/// S-035の生SQLクエリーをSQLxで実行する。
async fn s035_raw(db: &sqlx::PgPool) -> anyhow::Result<Vec<S035Row>> {
    sqlx::query_as!(
        S035Row,
        r#"
            WITH customer_amount AS (
                SELECT
                    customer_id,
                    SUM(amount) AS sum_amount
                FROM receipt
                WHERE
                    customer_id NOT LIKE 'Z%'
                GROUP BY customer_id
            )
            SELECT
                customer_id,
                sum_amount
            FROM customer_amount
            WHERE
                sum_amount >= (
                    SELECT
                        AVG(sum_amount)
                    FROM customer_amount
                )
            LIMIT 10"#
    )
    .fetch_all(db)
    .await
    .with_context(|| "raw query")
}

/// S-035のSQL文と同様のものをSeaQueryで構築し、SQLxで実行する。
async fn s035_sea_query(db: &sqlx::PgPool) -> anyhow::Result<Vec<S035Row>> {
    use ReceiptIden::*;

    // 一時的なテーブルやカラムの名前
    let customer_amount = Alias::new("customer_amount");
    let sum_amount = Alias::new("sum_amount");

    // WITH句。このWITH句は共通表式（CTE）を1つ持つ
    let with = WithClause::new()
        .cte(
            CommonTableExpression::new()
                .table_name(customer_amount.clone())
                .query(
                    Query::select()
                        .column(CustomerId)
                        .expr_as(Func::sum(Expr::col(Amount)), sum_amount.clone())
                        .from(ReceiptIden::Table)
                        .and_where(Expr::col(CustomerId).not_like("Z%"))
                        .group_by_col(CustomerId)
                        .to_owned(),
                )
                .to_owned(),
        )
        .to_owned();

    // 本体のSELECT文
    let select = Query::select()
        .column(CustomerId)
        .column(sum_amount.clone())
        .from(customer_amount.clone())
        .and_where(
            Expr::col(sum_amount.clone()).gte(sub_query(
                Query::select()
                    .expr(Func::avg(Expr::col(sum_amount)))
                    .from(customer_amount),
            )),
        )
        .limit(10)
        .to_owned();

    // SQL文を生成しSQLxで実行する
    let (sql, values) = select.with(with).build_sqlx(PostgresQueryBuilder);
    sqlx::query_as_with(&sql, values)
        .fetch_all(db)
        .await
        .with_context(|| format!("sea-query: {}", sql))
}

/// S-035のクエリー結果を格納する構造体
#[derive(sqlx::FromRow, Debug, PartialEq, Eq)]
pub struct S035Row {
    pub customer_id: Option<String>,
    pub sum_amount: Option<i64>,
}

impl FieldNames for S035Row {
    fn field_names() -> &'static [&'static str] {
        &["customer_id", "sum_amount"]
    }
}

impl fmt::Display for S035Row {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}, {}",
            self.customer_id.clone().unwrap_or_default(),
            self.sum_amount.unwrap_or_default(),
        )
    }
}

// ---------------------------------------------------
// S-036
// ---------------------------------------------------

/// S-036の生SQLクエリーとSeaQueryで構築したクエリーを実行し、結果が同じであることを確認する。
async fn s036(db: &sqlx::PgPool) -> anyhow::Result<()> {
    println!("S-036");
    let rows1 = s036_raw(db).await?;
    let rows2 = s036_sea_query(db).await?;

    assert_eq!(rows1.len(), rows2.len());
    println!("{}", S036Row::field_names().join(", "));
    for (row1, row2) in rows1.into_iter().zip(rows2) {
        println!("{row2}");
        assert_eq!(row1, row2);
    }
    println!();

    Ok(())
}

/// S-035の生SQLクエリーをSQLxで実行する。
async fn s036_raw(db: &sqlx::PgPool) -> anyhow::Result<Vec<S036Row>> {
    sqlx::query_as!(
        S036Row,
        r#"
            SELECT
                r.*,
                s.store_name
            FROM receipt r
            JOIN store s
            ON
                r.store_cd = s.store_cd
            LIMIT 10"#
    )
    .fetch_all(db)
    .await
    .with_context(|| "raw query")
}

/// S-036のSQL文と同様のものをSeaQueryで構築し、SQLxで実行する。
async fn s036_sea_query(db: &sqlx::PgPool) -> anyhow::Result<Vec<S036Row>> {
    use ReceiptIden as R;
    use StoreIden as S;

    let (sql, values) = Query::select()
        .column((R::Table, Asterisk))
        .column((S::Table, S::StoreName))
        .from(R::Table)
        .inner_join(
            S::Table,
            Expr::col((R::Table, R::StoreCd)).equals((S::Table, S::StoreCd)),
        )
        .limit(10)
        .build_sqlx(PostgresQueryBuilder);

    // SQL文をSQLxで実行する
    sqlx::query_as_with(&sql, values)
        .fetch_all(db)
        .await
        .with_context(|| format!("sea-query: {}", sql))
}

/// S-036のクエリー結果を格納する構造体
#[derive(sqlx::FromRow, Debug, PartialEq, Eq)]
struct S036Row {
    sales_ymd: i32,
    sales_epoch: Option<i32>,
    store_cd: String,
    receipt_no: i16,
    receipt_sub_no: i16,
    customer_id: Option<String>,
    product_cd: Option<String>,
    quantity: Option<i32>,
    amount: Option<i32>,
    store_name: Option<String>,
}

impl FieldNames for S036Row {
    fn field_names() -> &'static [&'static str] {
        &[
            "sales_ymd",
            "sales_epoch",
            "store_cd",
            "receipt_no",
            "receipt_sub_no",
            "customer_id",
            "product_cd",
            "quantity",
            "amount",
            "store_name",
        ]
    }
}

impl fmt::Display for S036Row {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}",
            self.sales_ymd,
            self.sales_epoch.unwrap_or_default(),
            self.store_cd,
            self.receipt_no,
            self.receipt_sub_no,
            self.customer_id.clone().unwrap_or_default(),
            self.product_cd.clone().unwrap_or_default(),
            self.quantity.unwrap_or_default(),
            self.amount.unwrap_or_default(),
            self.store_name.clone().unwrap_or_default()
        )
    }
}

// ---------------------------------------------------
// 補助関数
// ---------------------------------------------------

/// `SELECT`文をサブクエリーに変換する。
fn sub_query(select: &mut SelectStatement) -> SimpleExpr {
    SimpleExpr::SubQuery(None, Box::new(select.take().into_sub_query_statement()))
}
